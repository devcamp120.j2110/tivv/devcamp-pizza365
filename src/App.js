
import "bootstrap/dist/css/bootstrap.css";
import Carousel from 'react-bootstrap/Carousel'


import './App.css'
import image1 from './assets/images/1.jpg'
import image2 from './assets/images/2.jpg'
import image3 from './assets/images/3.jpg'
import image4 from './assets/images/4.jpg'
import seafoodImage from "./assets/images/seafood.jpg"
import hawaiiImage from "./assets/images/hawaiian.jpg"
import baconImage from "./assets/images/bacon.jpg"
const CSS = {
  possition: {
    position: "relative",
    bottom: "0px",
    width: "100%"
  }
}
function App() {
  return (
    <>
      <header>
        {/* <!-- 1.1 Header - Navbar menu --> */}
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                  <ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-fill w-100">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">Trang chủ </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Combo</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Loại Pizza</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Gửi đơn hàng</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
        {/* <!-- 1.2 Header - Carosel --> */}
        <div class="container mt-5">
          <h1 class="text-warning text-uppercase font-weight-bold pt-5">Pizza 365</h1>
          <h6 class="text-warning font-italic">Truly Italian</h6>
          <Carousel>
            <Carousel.Item>
              <img className="d-block w-100" src={image1} alt="First slide" />
            </Carousel.Item>
            <Carousel.Item>
              <img className="d-block w-100" src={image2} alt="Second slide" />
            </Carousel.Item>
            <Carousel.Item>
              <img className="d-block w-100" src={image3} alt="Third slide" />
            </Carousel.Item>
          </Carousel>
        </div>
      </header>
      {/* <!-- Body - content --> */}
      <section>
        <div class="container mt-5">
          {/* <!---giới thiệu--> */}
          <p class="text-center h3 text-warning">Tại sao lại Pizza 365 </p>
          <hr class="border-warning" />
          <div class="row p-2">
            <div class="col-3 intro-1">
              <div>
                <h4 class="font-weight-bold py-3">Đa dạng</h4>
              </div>
              <div>
                <p>Số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay</p>
              </div>
            </div>
            <div class="col-3 intro-2">
              <div>
                <h4 class="font-weight-bold py-3">Chất lượng</h4>
              </div>
              <div>
                <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm</p>
              </div>
            </div>
            <div class="col-3 intro-3">
              <div>
                <h4 class="font-weight-bold py-3">Hương vị</h4>
              </div>
              <div>
                <p>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm được từ Pizza 565</p>
              </div>
            </div>
            <div class="col-3 intro-4">
              <div>
                <h4 class="font-weight-bold py-3">Dịch vụ</h4>
              </div>
              <div>
                <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
              </div>
            </div>
          </div>
          {/* <!--Menu Combo--> */}
          <p class="text-center h3 text-warning pt-5">Chọn size Pizza </p>
          <hr class="border-warning" />
          <p class="text-center text-warning">Chọn combo Pizza phù hợp với nhu cầu của bạn </p>
          <div class="row">
            <div class="col-4 p-2 border-warning">
              <div class="card">
                <div class="card-header header-01 text-center font-weight-bolder">
                  S (small)
                </div>
                <div class="card-body text-center">
                  <p class="card-text">Đường kính: <span>20cm</span></p>
                  <hr />
                  <p class="card-text">Sườn nướng: <span>2</span></p>
                  <hr />
                  <p class="card-text">Salad: <span>200g</span></p>
                  <hr />
                  <p class="card-text">Nước ngọt: <span>2</span></p>
                  <hr />
                  <p class="card-text"><span class="h1">150.000</span><br />VNĐ</p>
                </div>
                <div class="card-footer text-muted">
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
            <div class="col-4 p-2 border-warning">
              <div class="card">
                <div class="card-header header-02 text-center font-weight-bolder">
                  M (medium)
                </div>
                <div class="card-body text-center">
                  <p class="card-text">Đường kính: <span>25cm</span></p>
                  <hr />
                  <p class="card-text">Sườn nướng: <span>4</span></p>
                  <hr />
                  <p class="card-text">Salad: <span>300g</span></p>
                  <hr />
                  <p class="card-text">Nước ngọt: <span>3</span></p>
                  <hr />
                  <p class="card-text"><span class="h1">200.000</span><br />VNĐ</p>
                </div>
                <div class="card-footer text-muted">
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
            <div class="col-4 p-2 border-warning">
              <div class="card">
                <div class="card-header header-03 text-center font-weight-bolder">
                  L (large)
                </div>
                <div class="card-body text-center">
                  <p class="card-text">Đường kính: <span>30cm</span></p>
                  <hr />
                  <p class="card-text">Sườn nướng: <span>8</span></p>
                  <hr />
                  <p class="card-text">Salad: <span>500g</span></p>
                  <hr />
                  <p class="card-text">Nước ngọt: <span>4</span></p>
                  <hr />
                  <p class="card-text"><span class="h1">250.000</span><br />VNĐ</p>
                </div>
                <div class="card-footer text-muted">
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
          </div>
          {/* <!--Loại Pizza--> */}
          <p class="text-center h3 text-warning pt-5">Chọn loại Pizza </p>
          <hr class="border-warning" />
          <div class="row pt-4">
            <div class="col-4">
              <div class="card">
                <img class="card-img-top" src={seafoodImage} alt="" />
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">OCEAN MANIA</h4>
                  <p class="card-text h6">PIZZA HẢI SẢN SỐT MAYONNAISE</p>
                  <p class="card-text">Sốt cà chua, phô mai, tôm, mực, thanh cua, hành tây.</p>
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="card">
                <img class="card-img-top" src={hawaiiImage} alt="" />
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">HAWAIIAN</h4>
                  <p class="card-text h6">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                  <p class="card-text">Sốt phô mai, cà chua, thịt dăm bông thơm.</p>
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="card">
                <img class="card-img-top" src={baconImage} alt="" />
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">CHEESY CHICKEN BACON</h4>
                  <p class="card-text h6fa-flip-horizontal">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                  <p class="card-text">Sốt phô mai, thịt gà, thịt heo muối, phô mai, cà chua.</p>
                  <button class="btn btn-warning w-100 font-weight-bold">Chọn</button>
                </div>
              </div>
            </div>
          </div>
          {/* <!--Chọn đồ uống--> */}
          <p class="text-center h3 text-warning pt-5">Chọn đồ uống </p>
          <hr class="border-warning" />
          <div class="row p-3">
            <select name="drink" id="sel-drink" class="form-control">
              <option value="All">Tất cả loại nước uống</option>
              <option value="0">Trà tắc</option>
              <option value="1">Pepsi</option>
              <option value="2">CocaCola</option>
              <option value="3">Trà sữa</option>
              <option value="4">Cà phê</option>
            </select>
          </div>
          {/* <!---Thông tin đơn hàng--> */}
          <p class="text-center h3 text-warning">Gửi đơn hàng </p>
          <hr class="border-warning" />
          <form>
            <div class="row py-4">
              <div class="col-12">
                <label for="">Tên</label>
                <input type="text" class="form-control w-100" placeholder="Nhập tên"></input>
                <label for="">Email</label>
                <input type="text" class="form-control w-100" placeholder="Nhập email"></input>
                <label for="">Địa chỉ</label>
                <input type="text" class="form-control w-100" placeholder="Nhập số điện thoại"></input>
                <label for="">Số điện thoại</label>
                <input type="text" class="form-control w-100" placeholder="Nhập địa chỉ"></input>
                <label for="">Mã giảm giá</label>
                <input type="text" class="form-control w-100" placeholder="Nhập mã giảm giá"></input>
                <label for="">Lời nhắn</label>
                <input type="text" class="form-control w-100" placeholder="Nhập lời nhắn"></input>
                <button class="btn btn-warning w-100 form-control my-4">Gửi</button>
              </div>
            </div>
          </form>
        </div>
      </section>
      {/* <!-- 4. Footer --> */}
      <footer>
        <div class="bg-warning text-center">
          <p class="font-weight-bold">Footer</p>
          <a href="#"><button class="btn btn-secondary"><i class="fas fa-arrow-up"></i> To the top</button></a>
          <p><i class="fab fa-facebook-square"></i>
            <i class="fab fa-instagram"></i>
            <i class="fab fa-snapchat"></i>
            <i class="fab fa-pinterest-p"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-linkedin-in"></i>
          </p>
          <p class="font-weight-bolder">Powered by DEVCAMP</p>
        </div>
      </footer>
    </>
  );
}

export default App;
